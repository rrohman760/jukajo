package com.jukajo.latihan.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@Builder
public class RegisterRequest {
    private String noKtp;
    private String npwp;
    private String tmptLahir;
    private LocalDateTime tglLahir;
    @Column(columnDefinition = "text")
    private String almtSurat;
    private String noWa;
    private String pinBb;
    private String nmGelarDepan;
    private String nmGelarBelakang;
    private String nmUser;
    private String userName;
    @JsonIgnore
    private String passwordUser; // beda type
    @Column(columnDefinition = "text")
    private String almtUser;
    private String telpUser;
    private String emailUser; // beda type
    private int levelUser;
    private int sttsUser;
    private String role;
}
