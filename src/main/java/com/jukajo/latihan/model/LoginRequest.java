package com.jukajo.latihan.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class LoginRequest {
    private String emailUser;
    private String passwordUser;
}
