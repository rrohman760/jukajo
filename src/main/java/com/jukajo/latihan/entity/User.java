package com.jukajo.latihan.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tbl_user")
public class User {
    @Id
    private String id;
    private String noKtp;
    private String npwp;
    private String tmptLahir;
    private LocalDateTime tglLahir;
    @Column(columnDefinition = "text")
    private String almtSurat;
    private String noWa;
    private String pinBb;
    private String nmGelarDepan;
    private String nmGelarBelakang;
    private String nmUser;
    private String userName;
    @JsonIgnore
    private String passwordUser; // beda type
    @Column(columnDefinition = "text")
    private String almtUser;
    private String telpUser;
    private String emailUser; // beda type
    private int levelUser;
    private int sttsUser;
    private String role;
}
