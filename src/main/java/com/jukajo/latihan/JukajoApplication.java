package com.jukajo.latihan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JukajoApplication {

	public static void main(String[] args) {
		SpringApplication.run(JukajoApplication.class, args);
	}

}
