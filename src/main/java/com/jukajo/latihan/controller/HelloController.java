package com.jukajo.latihan.controller;

import com.jukajo.latihan.security.UserPrincipal;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class HelloController {
    @GetMapping("/")
    public String sayHello(){
        return "Hello world";
    }
    @GetMapping("/secured")
    public String secured(@AuthenticationPrincipal UserPrincipal principal){
        return "If you this, your are login as user " + principal.getEmailUser() + " User id : " + principal.getUserId() + " role " + principal.getAuthorities();
    }
    @GetMapping("/admin")
    public String admin(@AuthenticationPrincipal UserPrincipal principal){
        return "If you this, your are login as user Admin";
    }
    @GetMapping("/user")
    public String user(@AuthenticationPrincipal UserPrincipal principal){
        return "If you this, your are login as user User";
    }
}
