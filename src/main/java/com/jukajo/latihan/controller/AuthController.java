package com.jukajo.latihan.controller;

import com.jukajo.latihan.model.LoginRequest;
import com.jukajo.latihan.model.AuthResponse;
import com.jukajo.latihan.model.RegisterRequest;
import com.jukajo.latihan.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    @Autowired
    private AuthService authService;
    @PostMapping("/login")
    public AuthResponse login(@RequestBody @Validated LoginRequest request) {
        return authService.attemptLogin(request.getEmailUser(), request.getPasswordUser());
    }
    @PostMapping("/register")
    public AuthResponse register(@RequestBody @Validated RegisterRequest request){
        return authService.attemptRegister(request);
    }
}