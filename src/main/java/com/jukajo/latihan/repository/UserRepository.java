package com.jukajo.latihan.repository;

import com.jukajo.latihan.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, String> {

    @Query("SELECT u FROM User u ORDER BY u.id DESC LIMIT 1")
    Optional<User> findTopByOrderByIdDesc();

    @Query("SELECT u FROM  User u WHERE u.emailUser = :email")
    Optional<User> findByEmailUser(@Param("email") String email);

    @Query("SELECT u FROM  User u WHERE u.emailUser = :email")
    boolean existsByEmailUser(@Param("email")String email);
}
