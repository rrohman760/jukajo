package com.jukajo.latihan.service;

import com.jukajo.latihan.entity.User;
import com.jukajo.latihan.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class UserServiceImpl {

    @Autowired
    private UserRepository userRepository;

    public String generateCustomId() {
        LocalDateTime now = LocalDateTime.now();
        String customId = String.format("%04d%02d%02d%02d%02d%s",
                now.getYear(),
                now.getMonthValue(),
                now.getDayOfMonth(),
                now.getHour(),
                now.getMinute(),
                generateIndex());
        return customId;
    }

    private String generateIndex() {
        Long lastIndex = userRepository.findTopByOrderByIdDesc()
                .map(entity -> Long.parseLong(entity.getId().substring(12)))
                .orElse(0L);
        String index = (lastIndex + 1 < 10) ? "0" + (lastIndex + 1 ) : String.valueOf(lastIndex + 1);
        return index;
    }

    private static final String EXISTING_EMAIL = "user@test.com";
    private static final String ANOTHER_EMAIL = "admin@test.com";
    public Optional<User> findByEmailUser(String email){
//        return userRepository.findByEmailUser(email);
        if (EXISTING_EMAIL.equalsIgnoreCase(email)){
            var user = new User();
            user.setId("20170110161409");
            user.setNoKtp("3604056112740001");
            user.setNpwp("80.420.802.3-416.000");
            user.setTmptLahir("Tanjungkarang");
            user.setTglLahir(LocalDateTime.parse("1974-12-21T00:00:00"));
            user.setAlmtSurat("Harjatani Heritage Blok C No.1 Margatani Kramatwatu  Kramatwatu Serang");
            user.setNoWa("089664976003");
            user.setPinBb("2a58e77e");
            user.setNmUser("Zulhaida..Bidan MOU -B");
            user.setPasswordUser("$2a$12$Ur18gzFS24d51z12LFpmzeg3tlgyx0fuIgkKaeMjkofC3K.kkZPLq"); //123456
            user.setAlmtUser("Griya Serdang Indah Blok B1 No.5 Margatani Kramatwatu Serang");
            user.setTelpUser("8111901375");
            user.setEmailUser(EXISTING_EMAIL);
            user.setLevelUser(3);
            user.setSttsUser(1);
            user.setRole("ROLE_USER");
            return Optional.of(user);
        } else if (ANOTHER_EMAIL.equalsIgnoreCase(email)) {
            var user = new User();
            user.setId("20240110161410");
            user.setNoKtp("3604056112740011");
            user.setNpwp("80.420.802.3-416.001");
            user.setTmptLahir("Tanjungkarang");
            user.setTglLahir(LocalDateTime.parse("1974-12-21T00:00:00"));
            user.setAlmtSurat("Harjatani Heritage Blok C No.1 Margatani Kramatwatu  Kramatwatu Serang");
            user.setNoWa("089664976003");
            user.setPinBb("2a58e77e");
            user.setNmUser("Zulhaida..Bidan MOU -B");
            user.setPasswordUser("$2a$12$Ur18gzFS24d51z12LFpmzeg3tlgyx0fuIgkKaeMjkofC3K.kkZPLq"); //123456
            user.setAlmtUser("Griya Serdang Indah Blok B1 No.5 Margatani Kramatwatu Serang");
            user.setTelpUser("8111901375");
            user.setEmailUser(ANOTHER_EMAIL);
            user.setLevelUser(3);
            user.setSttsUser(1);
            user.setRole("ROLE_ADMIN");
            return Optional.of(user);
        }
        return Optional.empty();


    }
}
