package com.jukajo.latihan.service;

import com.jukajo.latihan.entity.User;
import com.jukajo.latihan.model.AuthResponse;
import com.jukajo.latihan.model.RegisterRequest;
import com.jukajo.latihan.repository.UserRepository;
import com.jukajo.latihan.security.JwtIssuer;
import com.jukajo.latihan.security.UserPrincipal;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {
    @Autowired
    private JwtIssuer jwtIssuer;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;

    public AuthResponse attemptLogin(String email, String password) {
        var authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(email, password)
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        var principal = (UserPrincipal) authentication.getPrincipal();
        var roles = principal.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .toList();
        var token = jwtIssuer.issue(principal.getUserId(), principal.getEmailUser(), roles);

        return AuthResponse.builder()
                .accessToken(token)
                .build();
    }

    public AuthResponse attemptRegister(RegisterRequest request){
        if (userRepository.existsByEmailUser(request.getEmailUser())){
        // todo use dto ?
        }
        var generateId = userService.generateCustomId();
        var user = User.builder()
                .id(generateId)
                .noKtp(request.getNoKtp())
                .npwp(request.getNpwp())
                .tmptLahir(request.getTmptLahir())
                .tglLahir(request.getTglLahir())
                .almtSurat(request.getAlmtSurat())
                .noWa(request.getNoWa())
                .pinBb(request.getPinBb())
                .nmGelarDepan(request.getNmGelarDepan())
                .nmGelarBelakang(request.getNmGelarBelakang())
                .nmUser(request.getNmUser())
                .userName(generateId)
                .passwordUser(passwordEncoder.encode(request.getPasswordUser()))
                .almtUser(request.getAlmtUser())
                .levelUser(request.getLevelUser())
                .sttsUser(request.getSttsUser())
                .role(request.getRole())
                .build();

        userRepository.save(user);

        return attemptLogin(request.getEmailUser(), request.getPasswordUser());
    }

}
