package com.jukajo.latihan.security;

import com.jukajo.latihan.service.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    @Autowired
    private UserServiceImpl userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = userService.findByEmailUser(username).orElseThrow();
        return UserPrincipal.builder()
                .userId(user.getId())
                .emailUser(user.getEmailUser())
                .passwordUser(user.getPasswordUser())
                .authorities(List.of(new SimpleGrantedAuthority(user.getRole())))
                .build();
    }
}
